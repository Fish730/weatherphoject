package telros.searchdialog.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.view.WindowManager;

import telros.searchdialog.R;

public class UtilsDialog {
    public static AlertDialog.Builder createAlertDialog(Activity mActivity){
        return new AlertDialog.Builder(mActivity, R.style.AppTheme_Dialog);
    }

    public static void hideKeyboard(Activity activity) {
        activity.getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );
    }
}
