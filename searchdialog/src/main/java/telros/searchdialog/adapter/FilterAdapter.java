package telros.searchdialog.adapter;

import android.view.View;

import java.util.List;

import telros.searchdialog.R;
import telros.searchdialog.base.BaseRecyclerAdapter;
import telros.searchdialog.holder.FilterHolder;
import telros.searchdialog.model.FilterItem;

public class FilterAdapter extends BaseRecyclerAdapter<FilterHolder, FilterItem> {

    public FilterAdapter(List<FilterItem> cacheData) {
        super(cacheData);
    }

    @Override
    public int getLayoutResource() {
        return R.layout.items_view_layout;
    }

    @Override
    public void onCustomBindViewHolder(FilterHolder holder, FilterItem model, int position) {
        holder.bind(model);
    }

    @Override
    public FilterHolder onCustomCreateViewHolder(View view) {
        return new FilterHolder(view);
    }

    @Override
    public boolean isMainListenerDisable() {
        return false;
    }

    @Override
    public boolean isSelectable() {
        return false;
    }

    @Override
    public void onClickListener(View view, int position) {
        selectEvent(position);
        if (getListener() != null)
            getListener().onItemClick(view, position);
    }

    public void filter(String text) {
        getAllData().clear();
        if(text != null && !text.isEmpty()){
            text = text.toLowerCase();
            for(FilterItem item: getCacheData()){
                if(item.getDescription().toLowerCase().contains(text)){
                    getAllData().add(item);
                }
            }
        }else{
            getAllData().addAll(getCacheData());
        }
        notifyDataSetChanged();
    }
}
