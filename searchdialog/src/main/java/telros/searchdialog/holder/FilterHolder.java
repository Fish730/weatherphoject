package telros.searchdialog.holder;

import android.util.TypedValue;
import android.view.View;
import android.widget.TextView;

import telros.searchdialog.R;
import telros.searchdialog.base.BaseHolder;
import telros.searchdialog.model.FilterItem;


/**
 * Created by Barış ATALAY on 12.01.2018.
 */

public class FilterHolder extends BaseHolder<FilterItem> implements View.OnClickListener{
    private TextView description;

    public FilterHolder(View itemView) {
        super(itemView);
        description = itemView.findViewById(R.id.description);
    }

    @Override
    public void bind(FilterItem model) {
        description.setText(model.getDescription());
        int padding = description.getPaddingLeft();
        if (getListener() != null) {
            boolean selected = getListener().isSelected(model);
/*            if (selected)
                titleTxt.setBackgroundResource(R.color.filterdialog_row_selected);
            else{*/
                TypedValue typedValue = new TypedValue();
                getContext().getTheme().resolveAttribute(R.attr.selectableItemBackground, typedValue, true);
            description.setBackgroundResource(typedValue.resourceId);
  //          }
        }

        description.setPadding(padding,padding,padding,padding);

        description.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (getListener() != null)
            getListener().onClickListener(view, getLayoutPosition());
    }
}
