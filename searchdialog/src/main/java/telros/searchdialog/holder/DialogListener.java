package telros.searchdialog.holder;

import telros.searchdialog.model.FilterItem;

public interface DialogListener {
    void onResult(FilterItem selectedItem);
}
