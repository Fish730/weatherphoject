package telros.searchdialog.holder;

import android.annotation.SuppressLint;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.jakewharton.rxbinding2.widget.RxTextView;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import telros.searchdialog.R;
import telros.searchdialog.adapter.FilterAdapter;
import telros.searchdialog.model.AdapterListener;
import telros.searchdialog.model.FilterItem;

public class DialogHolder extends RecyclerView.ViewHolder implements AdapterListener, View.OnClickListener {
    private ImageView mBackButton;
    private MaterialSearchView mSearchView;
    private EditText mSearchTextView;
    private RecyclerView filterRecycler;

    private DialogListener listenerSingle;
    private FilterAdapter filterAdapter;

    @SuppressLint("CheckResult")
    public DialogHolder(View itemView) {
        super(itemView);
        initUi(itemView);

        LinearLayoutManager layoutManager = new LinearLayoutManager(itemView.getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        layoutManager.scrollToPosition(0);

        filterRecycler.setLayoutManager(layoutManager);
        filterRecycler.setHasFixedSize(true);
        filterRecycler.setItemAnimator(new DefaultItemAnimator());
        RxTextView.textChanges(mSearchTextView)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(charSequence -> filterAdapter.filter(charSequence.toString()));
    }

    @SuppressLint("CutPasteId")
    private void initUi(View itemView) {
        mSearchView = itemView.findViewById(R.id.search_view);
        mBackButton = mSearchView.findViewById(R.id.action_up_btn);
        mSearchTextView = mSearchView.findViewById(R.id.searchTextView);

        mSearchView.showSearch(false);
        filterRecycler = itemView.findViewById(R.id.filterList);
        filterRecycler.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(@NonNull RecyclerView rv, @NonNull MotionEvent e) {
                mSearchView.clearFocus();
                return false;
            }

            @Override
            public void onTouchEvent(@NonNull RecyclerView rv, @NonNull MotionEvent e) {
            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {
            }
        });
        mSearchView.clearFocus();
    }

    public void setListener(DialogListener listenerSingle) {
        this.listenerSingle = listenerSingle;
    }

    public DialogListener getListener() {
        return listenerSingle;
    }

    public void setFilterList(List<FilterItem> filterList) {
        filterRecycler.setAdapter(createFilterAdapter(filterList));
    }

    private FilterAdapter createFilterAdapter(List<FilterItem> filterList) {
        filterAdapter = new FilterAdapter(filterList);
        filterAdapter.setListener(this);
        return filterAdapter;
    }

    public void setOnCloseListener(View.OnClickListener onCloseListener) {
        mBackButton.setOnClickListener(onCloseListener);
    }

    public void setTextSearchView(String text) {
        mSearchTextView.setText(text);
    }

    @Override
    public void onItemClick(View view, int position) {
        if(listenerSingle != null)
            listenerSingle.onResult(filterAdapter.getItemFromPosition(position));
    }

    @Override
    public void onClick(View view) {

    }

}
