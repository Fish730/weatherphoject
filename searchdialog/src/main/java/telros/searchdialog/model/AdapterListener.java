package telros.searchdialog.model;

import android.view.View;

public interface AdapterListener {
    void onItemClick(View view, int position);
}
