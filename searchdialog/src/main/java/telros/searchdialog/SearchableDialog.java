package telros.searchdialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.List;

import telros.searchdialog.holder.DialogHolder;
import telros.searchdialog.holder.DialogListener;
import telros.searchdialog.model.FilterItem;
import telros.searchdialog.utils.UtilsDialog;

public class SearchableDialog implements View.OnClickListener  {

    private Activity mActivity;
    private AlertDialog.Builder alertDialogBuilder;
    private AlertDialog alertDialog;
    private List<FilterItem> filterList;
    private DialogHolder dialogHolder;

    private String textSearchView;

    public SearchableDialog(Activity activity) {
        this.mActivity = activity;
        this.alertDialogBuilder = UtilsDialog.createAlertDialog(mActivity);
        this.filterList = new ArrayList<>();
        backPressedEnabled(true);
    }

    public void setList(List<FilterItem> filterList){
        this.filterList = filterList;
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.action_up_btn)
            dispose();
    }

    public void show(DialogListener dialogListener){
        createDialogHolder();
        dialogHolder.setListener(dialogListener);
        setDefaultParameters();
        dialogHolder.setFilterList(filterList);
        alertDialog = alertDialogBuilder.show();
        Window window = alertDialog.getWindow();
        if (window != null)
            window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
    }

    private void createDialogHolder() {
        RelativeLayout view = new RelativeLayout(mActivity);
        LayoutInflater.from(mActivity).inflate(R.layout.search_dialog_view, view, true);
        dialogHolder = new DialogHolder(view);
        alertDialogBuilder.setView(dialogHolder.itemView);
    }

    private void setDefaultParameters() {
        dialogHolder.setOnCloseListener(this);
        dialogHolder.setTextSearchView(textSearchView);
    }

    public void backPressedEnabled(boolean value) {
        alertDialogBuilder.setCancelable(value);
    }

    public void dispose(){
        clear();
        UtilsDialog.hideKeyboard(mActivity);
    }

    private void clear() {
        if(alertDialog != null) {
            alertDialog.dismiss();
            alertDialog = null;
            alertDialogBuilder = null;
        }
    }

    public void setTextSearchView(String textSearchView) {
        this.textSearchView = textSearchView;
    }

}
