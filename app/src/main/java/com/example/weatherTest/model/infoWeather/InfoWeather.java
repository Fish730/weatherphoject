package com.example.weatherTest.model.infoWeather;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import com.example.weatherTest.model.list.ElementWeather;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

@Entity(tableName = "info_weather", indices = {@Index(value = {"nameCity", "oneday"},
        unique = true)})

public class InfoWeather {

    @PrimaryKey(autoGenerate = true)
    private int id_local;

    @ColumnInfo(name = "dateLoad")
    private String dateLoad;

    @ColumnInfo(name = "nameCity")
    private String nameCity;

    public void setList(ArrayList<ElementWeather> list) {
        this.list = list;
    }

    @ColumnInfo(name = "stringlist")
    private String stringlist;

    public ArrayList<ElementWeather> getList() {
        return list;
    }

    @Ignore
    private ArrayList<ElementWeather> list;

    @ColumnInfo(name = "oneday")
    private Boolean oneday;

    public Boolean getOneday() {
        return oneday;
    }

    public void setOneday(Boolean oneday) {
        this.oneday = oneday;
    }

    public String getDateLoad() {
        return dateLoad;
    }

    public String getNameCity() {
        return nameCity;
    }

    public void setNameCity(@NotNull String nameCity) {
        this.nameCity = nameCity;
    }

    public String getStringlist() {
        return stringlist;
    }

    public void setStringlist(String stringlist) {
        this.stringlist = stringlist;
    }

    public int getId_local() {
        return id_local;
    }

    public void setId_local(int id_local) {
        this.id_local = id_local;
    }

    public void setDateLoad(String dateLoad) {
        this.dateLoad = dateLoad;
    }

}
