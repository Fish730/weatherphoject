package com.example.weatherTest.model.city;


import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity(tableName = "city_list")
public class CityID {

    @Ignore
    private City city;

    @PrimaryKey(autoGenerate = true)
    private int id_local;

    @ColumnInfo(name = "stringVersionCity")
    private String stringVersionCity;


    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public int getId_local() {
        return id_local;
    }

    public void setId_local(int id_local) {
        this.id_local = id_local;
    }

    public String getStringVersionCity() {
        return stringVersionCity;
    }

    public void setStringVersionCity(String stringVersionCity) {
        this.stringVersionCity = stringVersionCity;
    }
}
