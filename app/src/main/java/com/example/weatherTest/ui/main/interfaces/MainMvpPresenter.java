package com.example.weatherTest.ui.main.interfaces;


import com.example.weatherTest.ui.base.interfaces.BaseMvpPresenter;

public interface MainMvpPresenter<V extends MainMvpView, I extends MainMvpInteractor>
        extends BaseMvpPresenter<V, I> {

    void checkCitiesList();

    String checkLastSelectCity();

    void loadInfoForHours(String nameCity);

    void loadInfoForSevenDays(String nameCity);

    void saveSelectCity(String cityName);

    void saveCityList(String cityIDS);

}
