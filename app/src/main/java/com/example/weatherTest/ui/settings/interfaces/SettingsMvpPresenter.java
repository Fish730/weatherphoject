package com.example.weatherTest.ui.settings.interfaces;

import com.example.weatherTest.ui.base.interfaces.BaseMvpPresenter;

public interface SettingsMvpPresenter<V extends SettingsMvpView, I extends SettingsMvpInteractor>
        extends BaseMvpPresenter<V, I> {

    void getCities();

}
