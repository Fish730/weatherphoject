package com.example.weatherTest.ui.base.interfaces;

import android.app.Activity;

public interface BaseMvpView {

    void showErrorDialog(String message);

    boolean isNetworkConnected();

    void hideSoftKeyboard(Activity activity);

    void showProgress(String message);

    void hideProgress();
}
