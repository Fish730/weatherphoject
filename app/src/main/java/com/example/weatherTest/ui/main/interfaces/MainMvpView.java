package com.example.weatherTest.ui.main.interfaces;


import com.example.weatherTest.model.infoWeather.InfoWeather;
import com.example.weatherTest.ui.base.interfaces.BaseMvpView;


public interface MainMvpView extends BaseMvpView {

    void showInfoWeatherHour(InfoWeather infoWeather);

    void showInfoWeatherSevenDays(InfoWeather infoWeather);

    void getListCities();

}
