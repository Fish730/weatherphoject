package com.example.weatherTest.ui.settings;

import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.example.weatherTest.R;
import com.example.weatherTest.model.city.City;
import com.example.weatherTest.model.city.CityID;
import com.example.weatherTest.ui.base.BaseActivity;
import com.example.weatherTest.ui.settings.interfaces.SettingsMvpInteractor;
import com.example.weatherTest.ui.settings.interfaces.SettingsMvpPresenter;
import com.example.weatherTest.ui.settings.interfaces.SettingsMvpView;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;

import org.jetbrains.annotations.NotNull;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import telros.searchdialog.SearchableDialog;
import telros.searchdialog.model.FilterItem;


public class SettingsActivity extends BaseActivity implements SettingsMvpView {

    @Inject
    public SettingsMvpPresenter<SettingsMvpView, SettingsMvpInteractor> mPresenter;

    @BindView(R.id.cityListArrayEditText)
    TextInputEditText cityListArrayEditText;

    String selectCityName = "";

    List<CityID> cityIDSArray = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        getActivityComponent().inject(this);
        mPresenter.onAttach(this);
        ButterKnife.bind(this);
        setUp();
        mPresenter.getCities();
    }

    @OnClick(R.id.cityListArrayEditText)
    void selectCityInSpinner() {
        List<FilterItem> filterItemsEmployees = new ArrayList<>();
        int iItem = 1;
        Gson gson = new Gson();
        for (CityID cities : this.cityIDSArray) {
            City city = gson.fromJson(cities.getStringVersionCity(), City.class);
            cities.setCity(city);
            filterItemsEmployees.add(new FilterItem(iItem++, city.getName()));
        }
        SearchableDialog searchableDialog = new SearchableDialog(this);
        searchableDialog.setList(filterItemsEmployees);
        searchableDialog.show(selectedItem -> {
            cityListArrayEditText.setText(selectedItem.getDescription());
            int id = selectedItem.getId();
            if (id != 0) {
                selectCityName = this.cityIDSArray.get((id - 1)).getCity().getName();
            }
            searchableDialog.dispose();
        });
    }

    @Override
    protected void setUp() {
        setTitle("Настройки");
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NotNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.putExtra("city", selectCityName);
        setResult(RESULT_OK, intent);
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    public void loadListCities(List<CityID> cityIDArrayList) {
        cityIDSArray = cityIDArrayList;
    }
}
