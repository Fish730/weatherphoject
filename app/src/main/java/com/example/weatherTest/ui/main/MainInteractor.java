package com.example.weatherTest.ui.main;


import com.example.weatherTest.data.database.dbHelper.interfaces.DbHelper;
import com.example.weatherTest.data.network.ApiHelper;
import com.example.weatherTest.data.preferences.PreferenceHelper;
import com.example.weatherTest.model.city.CityID;
import com.example.weatherTest.model.infoWeather.InfoWeather;
import com.example.weatherTest.ui.base.BaseInteractor;
import com.example.weatherTest.ui.main.interfaces.MainMvpInteractor;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Single;

public class MainInteractor extends BaseInteractor implements MainMvpInteractor {

    private ApiHelper appApiHelper;
    private DbHelper appDbHelper;
    private PreferenceHelper appPrefHelper;

    @Inject
    MainInteractor(DbHelper dbHelper, PreferenceHelper prefHelper, ApiHelper apiHelper) {
        super(prefHelper);
        appApiHelper = apiHelper;
        appDbHelper = dbHelper;
        appPrefHelper = prefHelper;
    }


    @Override
    public Single<List<CityID>> getListCityCallback() {
        return appDbHelper.selectCitiesList();
    }

    @Override
    public String getLastCityName() {
        return appPrefHelper.getLastCityName();
    }


    @Override
    public Single<Boolean> saveSelectCity(String cityName) {
        return appPrefHelper.saveSelectCity(cityName);
    }

    @Override
    public Single<Boolean> insertInfoWeather(InfoWeather infoWeather) {
        return appDbHelper.insertInfoWeather(infoWeather);
    }

    @Override
    public Single<InfoWeather> selectInfoWeatherByCityNameOneDay(String nameCity) {
        return appDbHelper.selectInfoWeatherByCityNameOneDay(nameCity);
    }

    @Override
    public Single<InfoWeather> selectInfoWeatherByCityNameSevenDays(String nameCity) {
        return appDbHelper.selectInfoWeatherByCityNameSevenDays(nameCity);
    }

    @Override
    public Observable<InfoWeather> getInfoWeatherFromServer(String cityName, int days) {
        return appApiHelper.getInfoWeather(cityName, "metric", days, "42f3619135fc6d51db80c63288cad323");
    }

    @Override
    public Single<Boolean> insertJsonCitiesArray(ArrayList<CityID> cityIDArrayList) {
        return appDbHelper.insertJsonCities(cityIDArrayList);
    }

    @Override
    public Single<Boolean> deleteAllCities() {
        return appDbHelper.deleteAllCitiesID();
    }

    @Override
    public ArrayList<CityID> arrayCityIdParse(String arrayCityIdString) {
        Gson gson = new Gson();
        CityID[] cityIDSArray = gson.fromJson(arrayCityIdString, CityID[].class);
        ArrayList<CityID> cityIDArrayList = new ArrayList<>(Arrays.asList(cityIDSArray));
        for (CityID cityID : cityIDArrayList) {
            cityID.setStringVersionCity(gson.toJson(cityID.getCity()));
        }
        return cityIDArrayList;
    }
}
