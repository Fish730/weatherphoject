package com.example.weatherTest.ui.settings;

import android.util.Log;

import com.example.weatherTest.ui.base.BasePresenter;
import com.example.weatherTest.ui.settings.interfaces.SettingsMvpInteractor;
import com.example.weatherTest.ui.settings.interfaces.SettingsMvpPresenter;
import com.example.weatherTest.ui.settings.interfaces.SettingsMvpView;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class SettingsPresenter<V extends SettingsMvpView, I extends SettingsMvpInteractor>
        extends BasePresenter<V, I>
        implements SettingsMvpPresenter<V, I> {

    @Inject
    SettingsPresenter(I mvpInteractor, CompositeDisposable compositeDisposable) {
        super(mvpInteractor, compositeDisposable);
    }

    @Override
    public void getCities() {
        getCompositeDisposable().add(
                getInteractor().getListCityCallback()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                cityList -> {
                                    getView().loadListCities(cityList);
                                }, throwable -> Log.d("Fish", "cityList throwable" + throwable.getMessage())
                        )
        );
    }

}
