package com.example.weatherTest.ui.settings;

import com.example.weatherTest.data.database.dbHelper.interfaces.DbHelper;
import com.example.weatherTest.data.network.ApiHelper;
import com.example.weatherTest.data.preferences.PreferenceHelper;
import com.example.weatherTest.model.city.CityID;
import com.example.weatherTest.ui.base.BaseInteractor;
import com.example.weatherTest.ui.settings.interfaces.SettingsMvpInteractor;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;

public class SettingsInteractor extends BaseInteractor implements SettingsMvpInteractor {

    private ApiHelper appApiHelper;
    private DbHelper appDbHelper;
    private PreferenceHelper appPrefHelper;

    @Inject
    SettingsInteractor(DbHelper dbHelper, PreferenceHelper prefHelper, ApiHelper apiHelper) {
        super(prefHelper);
        appApiHelper = apiHelper;
        appDbHelper = dbHelper;
        appPrefHelper = prefHelper;
    }


    @Override
    public Single<List<CityID>> getListCityCallback() {
        return appDbHelper.selectCitiesList();
    }

}
