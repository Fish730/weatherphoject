package com.example.weatherTest.ui.main.interfaces;


import com.example.weatherTest.model.city.City;
import com.example.weatherTest.model.city.CityID;
import com.example.weatherTest.model.infoWeather.InfoWeather;
import com.example.weatherTest.ui.base.interfaces.BaseMvpInteractor;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;

public interface MainMvpInteractor extends BaseMvpInteractor {


    Single<List<CityID>> getListCityCallback();

    String getLastCityName();

    Single<Boolean> saveSelectCity(String cityName);

    Single<Boolean> insertInfoWeather(InfoWeather infoWeather);

    Single<InfoWeather> selectInfoWeatherByCityNameOneDay(String nameCity);

    Single<InfoWeather> selectInfoWeatherByCityNameSevenDays(String nameCity);

    Observable<InfoWeather> getInfoWeatherFromServer(String cityName, int days);

    Single<Boolean> insertJsonCitiesArray(ArrayList<CityID> cityID);

    Single<Boolean> deleteAllCities();

    ArrayList<CityID> arrayCityIdParse(String arrayCityIdString);
}
