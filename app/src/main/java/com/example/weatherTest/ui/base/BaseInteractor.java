package com.example.weatherTest.ui.base;



import com.example.weatherTest.data.preferences.PreferenceHelper;

import javax.inject.Inject;


public class BaseInteractor {

    private final PreferenceHelper mPrefHelper;


    @Inject
    public BaseInteractor(PreferenceHelper appPrefHelper ) {
        mPrefHelper = appPrefHelper;
    }

}
