package com.example.weatherTest.ui.settings.interfaces;

import com.example.weatherTest.model.city.CityID;
import com.example.weatherTest.ui.base.interfaces.BaseMvpInteractor;

import java.util.List;

import io.reactivex.Single;

public interface SettingsMvpInteractor extends BaseMvpInteractor {

    Single<List<CityID>> getListCityCallback();

}
