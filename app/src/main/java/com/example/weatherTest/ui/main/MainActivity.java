package com.example.weatherTest.ui.main;


import androidx.appcompat.widget.Toolbar;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.weatherTest.R;

import com.example.weatherTest.model.infoWeather.InfoWeather;
import com.example.weatherTest.model.list.ElementWeather;
import com.example.weatherTest.ui.base.BaseActivity;

import com.example.weatherTest.ui.main.interfaces.MainMvpInteractor;
import com.example.weatherTest.ui.main.interfaces.MainMvpPresenter;
import com.example.weatherTest.ui.main.interfaces.MainMvpView;
import com.example.weatherTest.ui.settings.SettingsActivity;
import com.example.weatherTest.utils.AppConstant;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import lib.kingja.switchbutton.SwitchMultiButton;

public class MainActivity extends BaseActivity implements MainMvpView {


    private static final int REQUEST_CODE_SETTINGS = 1;
    private static final int REQUEST_CODE_PERMISSION_WRITE_INSTALL = 2;
    @Inject
    public MainMvpPresenter<MainMvpView, MainMvpInteractor> mPresenter;

    @BindView(R.id.settings)
    TextView settings;

    @BindView(R.id.selectCity)
    TextView selectCity;

    @BindView(R.id.containerScroll)
    LinearLayout containerScroll;

    @BindView(R.id.tempValue)
    TextView tempValue;

    @BindView(R.id.humidityValue)
    TextView humidityValue;

    @BindView(R.id.pressureValue)
    TextView pressureValue;

    @BindView(R.id.description)
    TextView description;

    @BindView(R.id.updateDate)
    TextView updateDate;

    @BindView(R.id.updateButton)
    TextView updateButton;

    @BindView(R.id.imageWeather)
    ImageView imageWeather;

    @BindView(R.id.switchmultibutton)
    SwitchMultiButton mSwitchMultiButton;


    private String nameCityFromShared;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_layout);
        getActivityComponent().inject(this);
        mPresenter.onAttach(this);
        ButterKnife.bind(this);
        setUp();
        mSwitchMultiButton.setOnSwitchListener((position, tabText) -> {
            if (tabText.equals(getResources().getStringArray(R.array.switch_tabs)[0])) {
                onClickOnHours();
            } else {
                onClickOnSevenDays();
            }
        });
    }

    @Override
    public void setUp() {
        setTitle("Прогноз погоды");
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mPresenter.checkCitiesList();
        nameCityFromShared = mPresenter.checkLastSelectCity();
        if (nameCityFromShared.equals("")) {
            selectCity.setText(getResources().getString(R.string.noSelectCity));
        } else {
            selectCity.setText(nameCityFromShared);
        }
        onClickOnHours();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!nameCityFromShared.equals("")) {
            updateButton.setVisibility(View.VISIBLE);
        } else {
            updateButton.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.settings)
    void startSettingsActivity() {
        Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
        startActivityForResult(intent, REQUEST_CODE_SETTINGS);
    }

    @OnClick(R.id.updateButton)
    void updateWeather() {
        if (mSwitchMultiButton.getSelectedTab() == 0) {
            onClickOnHours();
        } else {
            onClickOnSevenDays();
        }
    }

    private void onClickOnHours() {

        containerScroll.removeAllViews();
        Log.d("Fish", "ONCLICK on Hours");
        if (!nameCityFromShared.equals("")) {
            mPresenter.loadInfoForHours(this.nameCityFromShared);
        }
    }

    private void onClickOnSevenDays() {

        containerScroll.removeAllViews();
        if (!nameCityFromShared.equals("")) {
            mPresenter.loadInfoForSevenDays(this.nameCityFromShared);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_CODE_SETTINGS) {
                String nameCity = data.getStringExtra("city");
                String nameCityFromShared = mPresenter.checkLastSelectCity();
                assert nameCity != null;
                if ((!nameCity.equals(nameCityFromShared)) && (!nameCity.equals(""))) {
                    mPresenter.saveSelectCity(nameCity);
                    selectCity.setText(nameCity);
                    this.nameCityFromShared = nameCity;
                    if (mSwitchMultiButton.getSelectedTab() == 0) {
                        onClickOnHours();
                    } else {
                        onClickOnSevenDays();
                    }
                }
            }
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void showInfoWeatherHour(InfoWeather infoWeather) {

        updateDate.setText(getResources().getString(R.string.updateIn) + " " + infoWeather.getDateLoad());
        updateDate.setVisibility(View.VISIBLE);

        Gson gson = new Gson();
        String elementWeathersStringVersion = infoWeather.getStringlist();
        ElementWeather[] infoWeatherList = gson.fromJson(elementWeathersStringVersion, ElementWeather[].class);
        pressureValue.setText(infoWeatherList[0].getMain().getPressure() + " hPa");
        humidityValue.setText(infoWeatherList[0].getMain().getHumidity() + " %");
        description.setText(infoWeatherList[0].getWeather().get(0).getDescription());
        imageWeather.setImageResource(R.drawable.sun);
        tempValue.setText(infoWeatherList[0].getMain().getTemp().toString());

        /* Так как Api с подробной информацией, который я брал изнчально, не работает, то пришлось брать один Api для
        двух случаев. В нем нет данных о температуре, поэтому вставил вручную для визуальной состовляющей */

        containerScroll.addView(createViewForHoursWeather(17.0, getString(R.string.morning), R.drawable.sun));
        containerScroll.addView(createViewForHoursWeather(20.0, getString(R.string.day), R.drawable.day));
        containerScroll.addView(createViewForHoursWeather(19.0, getString(R.string.evening), R.drawable.afternoon));
        containerScroll.addView(createViewForHoursWeather(16.0, getString(R.string.night), R.drawable.moon));
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void showInfoWeatherSevenDays(InfoWeather infoWeather) {

        updateDate.setText(getResources().getString(R.string.updateIn) + " " + infoWeather.getDateLoad());
        updateDate.setVisibility(View.VISIBLE);

        Gson gson = new Gson();
        String elementWeathersStringVersion = infoWeather.getStringlist();
        ElementWeather[] infoWeatherList = gson.fromJson(elementWeathersStringVersion, ElementWeather[].class);

        for (int i = 0; i < infoWeatherList.length; i++) {
            containerScroll.addView(createViewForSevenDaysWeather(infoWeatherList[i]), i);
        }
        containerScroll.getChildAt(0).performClick();
    }

    @Override
    public void getListCities() {
        StringBuilder buf = new StringBuilder();
        InputStream json = null;
        try {
            json = getAssets().open(getString(R.string.nameFileCities));
        } catch (IOException e) {
            e.printStackTrace();
        }
        BufferedReader in = null;
        try {
            in = new BufferedReader(new InputStreamReader(json, "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String str = null;

        while (true) {
            try {
                assert in != null;
                if ((str = in.readLine()) == null) break;
            } catch (IOException e) {
                e.printStackTrace();
            }
            buf.append(str);
        }

        try {
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        String listCitiesString = String.valueOf(buf);
        mPresenter.saveCityList(listCitiesString);
    }

    @SuppressLint("SetTextI18n")
    private View createViewForHoursWeather(Double weatherValue, String weatherName, int sunIdDrawable) {
        View view = View.inflate(this, R.layout.weather_hours, null);
        TextView weatherNameView = view.findViewById(R.id.weatherName);
        TextView weatherValueView = view.findViewById(R.id.weatherValue);
        ImageView imageView = view.findViewById(R.id.imageWeather);
        imageView.setImageResource(sunIdDrawable);
        weatherNameView.setText(weatherName);
        String tempvalue = weatherValue + " " + getResources().getString(R.string.celTemp);
        weatherValueView.setText(tempvalue);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1);

        layoutParams.setMargins(50, 0, 0, 0);
        view.setLayoutParams(layoutParams);
        view.setOnClickListener(v -> {
            tempValue.setText(weatherValue.toString());
            imageWeather.setImageDrawable(imageView.getDrawable());
        });
        return view;
    }

    @SuppressLint("SetTextI18n")
    private View createViewForSevenDaysWeather(ElementWeather elementWeather) {

        long date = elementWeather.getDt();
        Double weatherValue = elementWeather.getMain().getTemp();
        View view = View.inflate(this, R.layout.weathersevendays, null);
        TextView weatherNameView = view.findViewById(R.id.weatherName);
        TextView weatherValueView = view.findViewById(R.id.weatherValue);
        ImageView imageView = view.findViewById(R.id.imageWeather);
        String url = AppConstant.URL_ICON_START + elementWeather.getWeather().get(0).getIcon() + AppConstant.URL_ICON_END;
        Glide.with(this).load(url).into(imageView);
        weatherNameView.setText(timeFromUTC(date));
        String tempvalue = weatherValue + " " + getResources().getString(R.string.celTemp);
        weatherValueView.setText(tempvalue);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1);

        layoutParams.setMargins(50, 0, 0, 0);
        view.setLayoutParams(layoutParams);
        view.setOnClickListener(v ->
        {
            tempValue.setText(weatherValue.toString());
            pressureValue.setText(elementWeather.getMain().getPressure() + " hPa");
            humidityValue.setText(elementWeather.getMain().getHumidity() + " %");
            description.setText(elementWeather.getWeather().get(0).getDescription());
            Glide.with(this).load(url).into(imageWeather);
        });
        return view;
    }

    //Конвертер Unix даты. При прогнозе на неделю, могут быть одинаковые даты, так как Api передает такие
    private String timeFromUTC(long date) {
        Calendar cal = Calendar.getInstance();
        TimeZone tz = cal.getTimeZone();
        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("dd/MM");
        sdf.setTimeZone(tz);
        return sdf.format(new Date(date * 1000));
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

}