package com.example.weatherTest.ui.base;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.example.weatherTest.ApplicationMVP;
import com.example.weatherTest.di.component.ActivityComponent;
import com.example.weatherTest.di.component.ApplicationComponent;


import com.example.weatherTest.di.component.DaggerActivityComponent;
import com.example.weatherTest.di.module.ActivityModule;
import com.example.weatherTest.ui.base.interfaces.BaseMvpView;
import com.example.weatherTest.utils.GeneralUtils;
import com.example.weatherTest.utils.NetworkUtils;
import com.leo.simplearcloader.ArcConfiguration;
import com.leo.simplearcloader.SimpleArcDialog;

public abstract class BaseActivity extends AppCompatActivity implements BaseMvpView {

    SimpleArcDialog mDialog;

    private ActivityComponent mActivityComponent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        resolveDaggerDependency();
    }

    protected void resolveDaggerDependency() {
        mActivityComponent = DaggerActivityComponent.builder()
                .activityModule(new ActivityModule(this))
                .applicationComponent(getApplicationComponent())
                .build();
    }

    public ActivityComponent getActivityComponent() {
        return mActivityComponent;
    }

    protected ApplicationComponent getApplicationComponent() {
        return ((ApplicationMVP) getApplication()).getApplicationComponent();
    }

    @Override
    public void showErrorDialog(String message) {
        GeneralUtils.showErrorDialog(this, message);
    }

    @Override
    public boolean isNetworkConnected() {
        return NetworkUtils.isNetworkConnected(getApplicationContext());
    }

    @Override
    public void hideSoftKeyboard(Activity activity) {
        GeneralUtils.hideSoftKeyboard(activity);
    }

    @Override
    public void showProgress(String message) {
        if (mDialog != null && mDialog.isShowing())
            mDialog.cancel();
        mDialog = new SimpleArcDialog(this);
        ArcConfiguration configuration = new ArcConfiguration(this);
        configuration.setColors(new int[]{Color.BLUE});
        if (message != null)
            configuration.setText(message);
        else
            configuration.setText("");
        mDialog.setConfiguration(configuration);
        mDialog.show();
        mDialog.setCancelable(false);
        mDialog.setCanceledOnTouchOutside(false);
    }

    @Override
    public void hideProgress() {
        if (mDialog != null && mDialog.isShowing()) {
            mDialog.cancel();
        }
    }

    protected abstract void setUp();
}
