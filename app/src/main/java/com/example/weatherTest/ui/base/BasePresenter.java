package com.example.weatherTest.ui.base;


import com.example.weatherTest.ui.base.interfaces.BaseMvpInteractor;
import com.example.weatherTest.ui.base.interfaces.BaseMvpPresenter;
import com.example.weatherTest.ui.base.interfaces.BaseMvpView;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class BasePresenter<V extends BaseMvpView, I extends BaseMvpInteractor>
        implements BaseMvpPresenter<V, I> {

    private V mView;
    private I mMvpInteractor;
    private final CompositeDisposable mCompositeDisposable;

    @Inject
    public BasePresenter(I mvpInteractor, CompositeDisposable compositeDisposable) {
        mMvpInteractor = mvpInteractor;
        mCompositeDisposable = compositeDisposable;

    }

    @Override
    public void onAttach(V mvpView) {
        mView = mvpView;
    }

    @Override
    public void onDetach() {
        mCompositeDisposable.dispose();
        mView = null;
        mMvpInteractor = null;
    }

    public CompositeDisposable getCompositeDisposable() {
        return mCompositeDisposable;
    }

    @Override
    public V getView() {
        return mView;
    }

    @Override
    public I getInteractor() {
        return mMvpInteractor;
    }
}
