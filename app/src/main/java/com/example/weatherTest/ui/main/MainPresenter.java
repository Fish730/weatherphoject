package com.example.weatherTest.ui.main;

import android.util.Log;


import com.example.weatherTest.model.city.City;
import com.example.weatherTest.model.city.CityID;
import com.example.weatherTest.ui.base.BasePresenter;
import com.example.weatherTest.ui.main.interfaces.MainMvpInteractor;
import com.example.weatherTest.ui.main.interfaces.MainMvpPresenter;
import com.example.weatherTest.ui.main.interfaces.MainMvpView;
import com.example.weatherTest.utils.AppConstant;
import com.example.weatherTest.utils.GeneralUtils;
import com.google.gson.Gson;

import java.util.ArrayList;

import javax.inject.Inject;


import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class MainPresenter<V extends MainMvpView, I extends MainMvpInteractor>
        extends BasePresenter<V, I>
        implements MainMvpPresenter<V, I> {

    @Inject
    MainPresenter(I mvpInteractor, CompositeDisposable compositeDisposable) {
        super(mvpInteractor, compositeDisposable);
    }

    @Override
    public void checkCitiesList() {
        getCompositeDisposable().add(
                getInteractor().getListCityCallback()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                cityList -> {
                                    if ((cityList.size() == 0) || (cityList.size() == 2)) {
                                        getView().getListCities();
                                    } else {
                                        Log.d("myLogFish", "Список городов имеется");
                                    }
                                }, throwable -> {
                                    Log.d("myLogFish", "cityList throwable" + throwable.getMessage());
                                    getView().getListCities();
                                }
                        )
        );
    }

    @Override
    public String checkLastSelectCity() {
        return getInteractor().getLastCityName();
    }

    @Override
    public void loadInfoForHours(String nameCity) {

        getCompositeDisposable().add(
                getInteractor().getInfoWeatherFromServer(nameCity, 1)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                infoWeather ->
                                {
                                    infoWeather.setNameCity(nameCity);
                                    infoWeather.setDateLoad(GeneralUtils.dateNow());
                                    infoWeather.setOneday(true);

                                    Gson gson = new Gson();
                                    String elementWeathersStringVersion = gson.toJson(infoWeather.getList());
                                    infoWeather.setStringlist(elementWeathersStringVersion);

                                    getCompositeDisposable().add(
                                            getInteractor().insertInfoWeather(infoWeather)
                                                    .subscribeOn(Schedulers.io())
                                                    .observeOn(AndroidSchedulers.mainThread())
                                                    .subscribe(
                                                            bolean -> {
                                                            },
                                                            throwable -> {
                                                                Log.d("myLogFish", "Ошибка вставки insertInfoWeather =" + throwable.getMessage());
                                                            }
                                                    )
                                    );
                                    getView().showInfoWeatherHour(infoWeather);
                                },
                                throwable ->
                                {
                                    Log.d("myLogFish", "getInfoWeatherFromServer =" + throwable.getMessage());
                                    getCompositeDisposable().add(
                                            getInteractor().selectInfoWeatherByCityNameOneDay(nameCity)
                                                    .subscribeOn(Schedulers.io())
                                                    .observeOn(AndroidSchedulers.mainThread())
                                                    .subscribe(
                                                            infoWeatherAnswer -> {
                                                                getView().showInfoWeatherHour(infoWeatherAnswer);
                                                            },
                                                            throwable1 -> {
                                                                Log.d("myLogFish", "Ошибка получения infoWeatherAnswer из БД" + throwable1.getMessage());
                                                            }
                                                    )
                                    );
                                }
                        )
        );
    }

    @Override
    public void loadInfoForSevenDays(String nameCity) {

        getCompositeDisposable().add(
                getInteractor().getInfoWeatherFromServer(nameCity, 7)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                infoWeather ->
                                {
                                    infoWeather.setNameCity(nameCity);
                                    infoWeather.setDateLoad(GeneralUtils.dateNow());
                                    infoWeather.setOneday(false);

                                    Gson gson = new Gson();
                                    String elementWeathersStringVersion = gson.toJson(infoWeather.getList());
                                    infoWeather.setStringlist(elementWeathersStringVersion);

                                    getCompositeDisposable().add(
                                            getInteractor().insertInfoWeather(infoWeather)
                                                    .subscribeOn(Schedulers.io())
                                                    .observeOn(AndroidSchedulers.mainThread())
                                                    .subscribe(
                                                            bolean -> {
                                                            },
                                                            throwable -> {
                                                                Log.d("myLogFish", "Ошибка вставки insertInfoWeather =" + throwable.getMessage());
                                                            }
                                                    )
                                    );
                                    getView().showInfoWeatherSevenDays(infoWeather);
                                },
                                throwable ->
                                {
                                    Log.d("myLogFish", "getInfoWeatherFromServer =" + throwable.getMessage());
                                    getCompositeDisposable().add(
                                            getInteractor().selectInfoWeatherByCityNameSevenDays(nameCity)
                                                    .subscribeOn(Schedulers.io())
                                                    .observeOn(AndroidSchedulers.mainThread())
                                                    .subscribe(
                                                            infoWeatherAnswer -> {
                                                                getView().showInfoWeatherSevenDays(infoWeatherAnswer);
                                                            },
                                                            throwable1 -> {
                                                                Log.d("myLogFish", "Ошибка получения infoWeatherAnswer из БД" + throwable1.getMessage());
                                                            }
                                                    )
                                    );
                                }
                        )
        );
    }

    private ArrayList<CityID> getDefaultListCities() {
        Gson gson = new Gson();

        CityID cityMoscow = new CityID();
        City cityMsc = new City(AppConstant.MoscowName);
        cityMoscow.setCity(cityMsc);
        cityMoscow.setStringVersionCity(gson.toJson(cityMsc));

        CityID citySpbId = new CityID();
        City cityspb = new City(AppConstant.SaintPetersburgName);
        citySpbId.setCity(cityspb);
        citySpbId.setStringVersionCity(gson.toJson(cityspb));

        ArrayList<CityID> cityIDArrayList = new ArrayList<>();
        cityIDArrayList.add(cityMoscow);
        cityIDArrayList.add(citySpbId);

        return cityIDArrayList;
    }

    @Override
    public void saveSelectCity(String cityName) {
        getCompositeDisposable().add(
                getInteractor().saveSelectCity(cityName)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                aBoolean -> {
                                }
                        )
        );
    }

    @Override
    public void saveCityList(String arrayCityIdString) {

        ArrayList<CityID> arrayListIdCity = getInteractor().arrayCityIdParse(arrayCityIdString);
        getCompositeDisposable().add(
                getInteractor().deleteAllCities()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                aBoolean -> {
                                    getCompositeDisposable().add(
                                            getInteractor().insertJsonCitiesArray(arrayListIdCity)
                                                    .subscribeOn(Schedulers.io())
                                                    .observeOn(AndroidSchedulers.mainThread())
                                                    .subscribe(
                                                            aBoolean1 -> {
                                                                Log.d("myLogFish", "Успешно вставил список городов");
                                                            },
                                                            throwable -> {
                                                                Log.d("myLogFish", "Ошибка вставки списка городов в БД" + throwable.getMessage());
                                                                getCompositeDisposable().add(
                                                                        getInteractor().insertJsonCitiesArray(getDefaultListCities())
                                                                                .subscribeOn(Schedulers.io())
                                                                                .observeOn(AndroidSchedulers.mainThread())
                                                                                .subscribe(
                                                                                        aBoolean2 -> {
                                                                                            Log.d("myLogFish", "Успешно вставил 2 дефолтных города");
                                                                                        },
                                                                                        throwable1 -> {
                                                                                            Log.d("myLogFish", "Ошибка вставки 2 городов" + throwable1.getMessage());
                                                                                        }
                                                                                )
                                                                );
                                                            }
                                                    )
                                    );
                                }
                        )
        );
    }
}
