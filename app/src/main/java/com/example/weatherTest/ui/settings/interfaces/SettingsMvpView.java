package com.example.weatherTest.ui.settings.interfaces;

import com.example.weatherTest.model.city.CityID;
import com.example.weatherTest.ui.base.interfaces.BaseMvpView;

import java.util.List;

public interface SettingsMvpView extends BaseMvpView {

    void loadListCities(List<CityID> cityIDArrayList);

}
