package com.example.weatherTest;

import android.app.Application;


import com.example.weatherTest.di.component.ApplicationComponent;

import com.example.weatherTest.di.component.DaggerApplicationComponent;
import com.example.weatherTest.di.module.ApplicationModule;


public class ApplicationMVP extends Application {
    private ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        applicationComponent = DaggerApplicationComponent
                .builder()
                .applicationModule(new ApplicationModule(this))
                .build();

    }

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }

}
