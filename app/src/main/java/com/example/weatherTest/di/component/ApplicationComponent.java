package com.example.weatherTest.di.component;


import com.example.weatherTest.data.database.dbHelper.interfaces.DbHelper;
import com.example.weatherTest.data.preferences.PreferenceHelper;
import com.example.weatherTest.di.module.ApplicationModule;

import javax.inject.Singleton;

import dagger.Component;
import retrofit2.Retrofit;


@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    Retrofit getRetrofit();
    DbHelper dbHelper();
    PreferenceHelper preferenceHelper();
}
