package com.example.weatherTest.di.module;

import androidx.appcompat.app.AppCompatActivity;

import com.example.weatherTest.data.network.ApiHelper;
import com.example.weatherTest.di.scope.PerActivity;
import com.example.weatherTest.ui.main.MainInteractor;
import com.example.weatherTest.ui.main.MainPresenter;
import com.example.weatherTest.ui.main.interfaces.MainMvpInteractor;
import com.example.weatherTest.ui.main.interfaces.MainMvpPresenter;
import com.example.weatherTest.ui.main.interfaces.MainMvpView;
import com.example.weatherTest.ui.settings.SettingsInteractor;
import com.example.weatherTest.ui.settings.SettingsPresenter;
import com.example.weatherTest.ui.settings.interfaces.SettingsMvpInteractor;
import com.example.weatherTest.ui.settings.interfaces.SettingsMvpPresenter;
import com.example.weatherTest.ui.settings.interfaces.SettingsMvpView;

import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;
import retrofit2.Retrofit;


@Module
public class ActivityModule {

    private AppCompatActivity mActivity;

    public ActivityModule(AppCompatActivity activity) {
        this.mActivity = activity;
    }

    @Provides
    AppCompatActivity provideActivity() {
        return mActivity;
    }


    @Provides
    CompositeDisposable provideCompositeDisposable() {
        return new CompositeDisposable();
    }


    @PerActivity
    @Provides
    ApiHelper provideApiService(Retrofit retrofit) {
        return retrofit.create(ApiHelper.class);
    }

    //PRESENTERS

    /**
     * Main
     */
    @PerActivity
    @Provides
    MainMvpPresenter<MainMvpView, MainMvpInteractor>
    provideMainPresenter(MainPresenter<MainMvpView, MainMvpInteractor> presenter) {
        return presenter;
    }
    /**
     * Settings
     */
    @PerActivity
    @Provides
    SettingsMvpPresenter<SettingsMvpView, SettingsMvpInteractor>
    provideSettingsPresenter(SettingsPresenter<SettingsMvpView, SettingsMvpInteractor> presenter) {
        return presenter;
    }

    //INTERACTORS

    /**
     * Main
     */
    @Provides
    @PerActivity
    MainMvpInteractor provideMainMvpInteractor(MainInteractor interactor) {
        return interactor;
    }

    /**
     * Settings
     */
    @Provides
    @PerActivity
    SettingsMvpInteractor provideSettingsMvpInteractor(SettingsInteractor interactor) {
        return interactor;
    }

}
