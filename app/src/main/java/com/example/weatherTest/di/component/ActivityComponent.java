package com.example.weatherTest.di.component;

import com.example.weatherTest.ui.main.MainActivity;
import com.example.weatherTest.di.module.ActivityModule;
import com.example.weatherTest.di.scope.PerActivity;
import com.example.weatherTest.ui.settings.SettingsActivity;

import dagger.Component;


@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {


    /**
     * MainActivity
     */
    void inject(MainActivity activity);


    /**
     * SettingsActivity
     */
    void inject(SettingsActivity activity);

}
