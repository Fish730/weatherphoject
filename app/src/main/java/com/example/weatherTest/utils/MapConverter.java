package com.example.weatherTest.utils;

import androidx.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Map;

public class MapConverter {

    @TypeConverter
    public String fromMap(Map<String, String> map) {
        if (map == null) {
            return (null);
        }
        Gson gson = new Gson();
        return gson.toJson(map);
    }

    @TypeConverter
    public Map<String, String> toMap(String s) {
        if (s == null) {
            return (null);
        }
        Type mapType = new TypeToken<Map<String, String>>() {
        }.getType();
        return new Gson().fromJson(s, mapType);
    }
}
