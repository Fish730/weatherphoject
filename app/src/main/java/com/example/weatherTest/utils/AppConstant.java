package com.example.weatherTest.utils;

public class AppConstant {

    public static String BASE_URL = "https://api.openweathermap.org/data/2.5/";
    public static final String DB_NAME = "vidDemoDb";
    public static final String PREF_NAME = "vidDemo_pref";
    public static String URL_ICON_START = "http://openweathermap.org/img/wn/";
    public static String URL_ICON_END = "@2x.png";

    public static String MoscowName = "Moscow";
    public static String SaintPetersburgName = "Saint Petersburg";

}
