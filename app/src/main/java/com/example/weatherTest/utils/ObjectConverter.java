package com.example.weatherTest.utils;

import androidx.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

public class ObjectConverter {

    @TypeConverter
    public String fromObject(List<Object> objectList) {
        if (objectList == null) {
            return (null);
        }
        Gson gson = new Gson();
        Type type = new TypeToken<List<Object>>() {}.getType();
        return gson.toJson(objectList, type);
    }

    @TypeConverter
    public List<Object> toObject(String s) {
        if (s == null) {
            return (null);
        }
        Gson gson = new Gson();
        Type type = new TypeToken<List<Object>>() {}.getType();
        return gson.fromJson(s, type);
    }

}
