package com.example.weatherTest.data.database.dbHelper.interfaces;


import com.example.weatherTest.model.city.CityID;
import com.example.weatherTest.model.infoWeather.InfoWeather;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Singleton;

import io.reactivex.Single;


@Singleton
public interface DbHelper {

    //CitiesID

    Single<List<CityID>> selectCitiesList();

    Single<Boolean> deleteAllCitiesID();

    Single<Boolean> insertJsonCities(ArrayList<CityID> cityIDArrayList);

    //InfoWeather

    Single<Boolean> insertInfoWeather(InfoWeather infoWeather);

    Single<InfoWeather> selectInfoWeatherByCityNameOneDay(String nameCity);

    Single<InfoWeather> selectInfoWeatherByCityNameSevenDays(String nameCity);

    Single<Boolean> deleteAllInfoWeather();
}
