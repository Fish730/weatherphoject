package com.example.weatherTest.data.database.dbHelper;


import com.example.weatherTest.data.database.AppDatabase;
import com.example.weatherTest.data.database.dbHelper.interfaces.DbHelper;
import com.example.weatherTest.model.city.CityID;
import com.example.weatherTest.model.infoWeather.InfoWeather;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;


public class AppDbHelper implements DbHelper {
    private final AppDatabase mAppDatabase;

    @Inject
    AppDbHelper(AppDatabase appDatabase) {
        this.mAppDatabase = appDatabase;
    }

    @Override
    public Single<Boolean> insertJsonCities(ArrayList<CityID> cityIDArrayList) {
        return Single.fromCallable(() -> {
            mAppDatabase.citiesDao().insertJsonity(cityIDArrayList);
            return true;
        });
    }

    @Override
    public Single<List<CityID>> selectCitiesList() {
        return Single.fromCallable(() -> mAppDatabase.citiesDao().selectAllCitiesID());
    }

    @Override
    public Single<Boolean> deleteAllCitiesID() {
        return Single.fromCallable(() -> {
            mAppDatabase.citiesDao().deleteAllCities();
            return true;
        });
    }


    @Override
    public Single<Boolean> insertInfoWeather(InfoWeather infoWeather) {
        return Single.fromCallable(() -> {
            mAppDatabase.infoWeatherDao().insertInfoWeather(infoWeather);
            return true;
        });
    }

    @Override
    public Single<InfoWeather> selectInfoWeatherByCityNameOneDay(String nameCity) {
        return Single.fromCallable(() -> mAppDatabase.infoWeatherDao().selectInfoWeatherByCityName(nameCity, true));
    }

    @Override
    public Single<InfoWeather> selectInfoWeatherByCityNameSevenDays(String nameCity) {
        return Single.fromCallable(() -> mAppDatabase.infoWeatherDao().selectInfoWeatherByCityName(nameCity, false));
    }

    @Override
    public Single<Boolean> deleteAllInfoWeather() {
        return Single.fromCallable(() -> {
            mAppDatabase.infoWeatherDao().deleteAllInfoWeather();
            return true;
        });
    }
}
