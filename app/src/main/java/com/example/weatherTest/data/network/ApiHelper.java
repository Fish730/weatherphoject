package com.example.weatherTest.data.network;


import com.example.weatherTest.model.infoWeather.InfoWeather;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;


public interface ApiHelper {

    @GET("find?lang=ru")
    Observable<InfoWeather> getInfoWeather(@Query("q") String cityName, @Query("units") String units, @Query("cnt") int days,@Query("APPID") String APP);
}
