package com.example.weatherTest.data.database.dao.cities;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.example.weatherTest.model.city.CityID;

import java.util.List;


@Dao
public interface CitiesDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertJsonity(List<CityID> cityIDS);

    @Query("SELECT * FROM city_list")
    List<CityID> selectAllCitiesID();

    @Query("DELETE FROM city_list")
    void deleteAllCities();

}
