package com.example.weatherTest.data.preferences;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.weatherTest.di.scope.PreferenceInfo;

import javax.inject.Inject;

import io.reactivex.Single;


public class AppPreferenceHelper implements PreferenceHelper {

    private SharedPreferences mPrefs;

    @Inject
    AppPreferenceHelper(Context context, @PreferenceInfo String prefName) {
        mPrefs = context.getSharedPreferences(prefName, Context.MODE_PRIVATE);
    }


    @Override
    public String getLastCityName() {
        return mPrefs.getString("selectCity", "");
    }

    @Override
    public Single<Boolean> saveSelectCity(String cityName) {
        return Single.fromCallable(() -> {
            mPrefs.edit().putString("selectCity", cityName).apply();
            return true;
        });
    }
}
