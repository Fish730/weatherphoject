package com.example.weatherTest.data.database.dao.infoWeather;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.example.weatherTest.model.infoWeather.InfoWeather;

@Dao
public interface InfoWeatherDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertInfoWeather(InfoWeather infoWeather);

    @Query("SELECT * FROM info_weather WHERE nameCity=:nameCity AND oneday=:oneday")
    InfoWeather selectInfoWeatherByCityName(String nameCity, boolean oneday);

    @Query("DELETE FROM info_weather")
    void deleteAllInfoWeather();
}
