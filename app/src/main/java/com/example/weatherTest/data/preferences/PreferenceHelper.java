package com.example.weatherTest.data.preferences;

import io.reactivex.Single;

public interface PreferenceHelper {

    String getLastCityName();

    Single<Boolean> saveSelectCity(String cityName);

}
