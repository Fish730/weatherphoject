package com.example.weatherTest.data.database;


import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.example.weatherTest.data.database.dao.cities.CitiesDao;
import com.example.weatherTest.data.database.dao.infoWeather.InfoWeatherDao;
import com.example.weatherTest.model.city.CityID;
import com.example.weatherTest.model.infoWeather.InfoWeather;


@Database(entities = {CityID.class, InfoWeather.class},
        version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {

    public abstract CitiesDao citiesDao();

    public abstract InfoWeatherDao infoWeatherDao();

}
